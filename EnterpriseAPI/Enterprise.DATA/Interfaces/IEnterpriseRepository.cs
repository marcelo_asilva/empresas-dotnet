﻿using EnterpriseDATA.Entities;
using System.Collections.Generic;

namespace EnterpriseDATA.Interfaces
{
    /// <summary>
    /// Interface for repositories responsible for loading the <see cref="TB_ENTERPRISES"/> data
    /// </summary>
    public interface IEnterpriseRepository
    {
        /// <summary>
        /// Filter the list of <see cref="TB_ENTERPRISES"/> by name
        /// </summary>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        List<TB_ENTERPRISES> FilterByName(string name);

        /// <summary>
        /// Filter the list of <see cref="TB_ENTERPRISES"/> by type
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <returns></returns>
        List<TB_ENTERPRISES> FilterByType(int typeid);

        /// <summary>
        /// Filter the list of <see cref="TB_ENTERPRISES"/> by type and/or name
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        List<TB_ENTERPRISES> FilterByTypeAndName(int typeid, string name);

        /// <summary>
        /// Lists all the <see cref="TB_ENTERPRISES"/> objetcs in the respository
        /// </summary>
        /// <returns></returns>
        List<TB_ENTERPRISES> GetAll();

        /// <summary>
        /// Find an <see cref="TB_ENTERPRISES"/> object by id
        /// </summary>
        /// <param name="id">ID number that identifies the <see cref="TB_ENTERPRISES"/> objetc</param>
        /// <returns></returns>
        TB_ENTERPRISES GetById(int id);
    }
}
