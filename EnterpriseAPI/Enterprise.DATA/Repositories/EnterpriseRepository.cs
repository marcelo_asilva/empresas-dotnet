﻿using EnterpriseDATA.Entities;
using EnterpriseDATA.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EnterpriseDATA.Repositories
{
    /// <summary>
    /// Repository responsible for loading the <see cref="TB_ENTERPRISES"/> data
    /// </summary>
    public class EnterpriseRepository : IEnterpriseRepository
    {
        // Class Variables
        #region Varibles
        /// <summary>
        /// Database source connection
        /// </summary>
        private DB_EnterpriseEntities _DataSource = new DB_EnterpriseEntities();
        #endregion

        // Constructors
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public EnterpriseRepository()
        {
            //
        }
        #endregion

        // Public methods
        #region Public
        /// <summary>
        /// Filter the data collection of <see cref="TB_ENTERPRISES"/> by name
        /// </summary>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        public List<TB_ENTERPRISES> FilterByName(string name)
        {
            // Gets the entity that have the searched name
            return _DataSource.TB_ENTERPRISES
                .OrderBy(e => e.id)
                .Where(e => e.enterprise_name.Equals(name, StringComparison.CurrentCultureIgnoreCase)).ToList();
        }

        /// <summary>
        /// Filter the data collection of <see cref="TB_ENTERPRISES"/> by type
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <returns></returns>
        public List<TB_ENTERPRISES> FilterByType(int typeid)
        {
            // Gets the entity that have the searched type
            return _DataSource.TB_ENTERPRISES
                .OrderBy(e => e.id)
                .Where(e => e.TB_ENTERPRISES_TYPE.id == typeid).ToList();
        }

        /// <summary>
        /// Filter the data collection of <see cref="TB_ENTERPRISES"/> by type and/or name
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        public List<TB_ENTERPRISES> FilterByTypeAndName(int typeid, string name = null)
        {
            // Gets the entity that have the searched type and name
            return _DataSource.TB_ENTERPRISES
                .OrderBy(e => e.id)
                .Where(e => e.TB_ENTERPRISES_TYPE.id == typeid
                && (name == null || e.enterprise_name.Equals(name, StringComparison.CurrentCultureIgnoreCase))).ToList();
        }

        /// <summary>
        /// Lists all the <see cref="TB_ENTERPRISES"/> objetcs in the respository
        /// </summary>
        /// <returns></returns>
        public List<TB_ENTERPRISES> GetAll()
        {
            // Gets and converts the entities
            return _DataSource.TB_ENTERPRISES.OrderBy(e => e.id).ToList();
        }

        /// <summary>
        /// Find an <see cref="TB_ENTERPRISES"/> object by id
        /// </summary>
        /// <param name="id">ID number that identifies the <see cref="TB_ENTERPRISES"/> objetc</param>
        /// <returns></returns>
        public TB_ENTERPRISES GetById(int id)
        {
            // If exists a entity with the id
            return _DataSource.TB_ENTERPRISES.FirstOrDefault(e => e.id == id);
        }
        #endregion

        // Destructors
        #region Destructors
        /// <summary>
        /// Descrutor
        /// </summary>
        ~EnterpriseRepository()
        {
            this._DataSource.Dispose();
        }
        #endregion
    }
}
