//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EnterpriseDATA.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_ENTERPRISES
    {
        public int id { get; set; }
        public string email_enterprise { get; set; }
        public string facebook { get; set; }
        public string twitter { get; set; }
        public string linkedin { get; set; }
        public string phone { get; set; }
        public bool own_enterprise { get; set; }
        public string enterprise_name { get; set; }
        public string photo { get; set; }
        public string description { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public Nullable<float> value { get; set; }
        public Nullable<float> share_price { get; set; }
        public int enterprise_type_id { get; set; }
        public Nullable<int> shares { get; set; }
        public Nullable<int> own_shares { get; set; }
    
        public virtual TB_ENTERPRISES_TYPE TB_ENTERPRISES_TYPE { get; set; }
    }
}
