﻿using EnterpriseDATA.Entities;
using EnterpriseDATA.Interfaces;
using EnterpriseDOMAIN.Interfaces;
using EnterpriseDOMAIN.Models;
using System;
using System.Collections.Generic;

namespace EnterpriseDOMAIN.Domain
{
    /// <summary>
    /// Domain responsible for dealing with the <see cref="Enterprise"/> data collection
    /// </summary>
    public class EnterpriseDomain : IEnterpriseDomain
    {
        // Variables
        #region Variables
        /// <summary>
        /// Repository responsible for loading the <see cref="TB_ENTERPRISES"/> data collection
        /// </summary>
        private IEnterpriseRepository _EnterpriseRepository;
        #endregion

        // Constructors
        #region Constructors
        /// <summary>
        /// Constructor using dependency injection on the repository
        /// </summary>
        /// <param name="enterpriseRepository">Domain responsible for dealing with the <see cref="TB_ENTERPRISES"/> data</param>
        public EnterpriseDomain(IEnterpriseRepository enterpriseRepository)
        {
            this._EnterpriseRepository = enterpriseRepository;
        }
        #endregion

        // Public Methods
        #region Public
        /// <summary>
        /// Filter the list of <see cref="Enterprise"/> by name
        /// </summary>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        public List<Enterprise> FilterByName(string name)
        {
            return this.EnterpriseTransformer(this._EnterpriseRepository.FilterByName(name));
        }

        /// <summary>
        /// Filter the list of <see cref="Enterprise"/> by type
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <returns></returns>
        public List<Enterprise> FilterByType(int typeid)
        {
            return this.EnterpriseTransformer(this._EnterpriseRepository.FilterByType(typeid));
        }

        /// <summary>
        /// Filter the list of <see cref="Enterprise"/> by type and/or name
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        public List<Enterprise> FilterByTypeAndName(int typeid, string name)
        {
            return this.EnterpriseTransformer(this._EnterpriseRepository.FilterByTypeAndName(typeid, name));
        }

        /// <summary>
        /// Lists all the <see cref="Enterprise"/> objetcs in the respository
        /// </summary>
        /// <returns></returns>
        public List<Enterprise> GetAll()
        {
            return this.EnterpriseTransformer(this._EnterpriseRepository.GetAll());
        }

        /// <summary>
        /// Find an <see cref="DetailedEnterprise"/> object by id
        /// </summary>
        /// <param name="id">ID number that identifies the <see cref="DetailedEnterprise"/> objetc</param>
        /// <returns></returns>
        public DetailedEnterprise GetById(int id)
        {
            return this.DetailedEnterpriseTransformer(this._EnterpriseRepository.GetById(id));
        }
        #endregion Private

        // Private methods
        #region Private
        /// <summary>
        /// Transforms a <see cref="TB_ENTERPRISES"/> entity into a <see cref="Enterprise"/> object
        /// </summary>
        /// <param name="enterprisedatasource"><see cref="TB_ENTERPRISES"/> to be transformed</param>
        /// <returns></returns>
        private DetailedEnterprise DetailedEnterpriseTransformer(TB_ENTERPRISES enterprisedatasource)
        {
            if (enterprisedatasource == null) return null;

            // Creates a new Enterprise object based on the data loaded from the database
            return new DetailedEnterprise()
            {
                ID = enterprisedatasource.id,
                EmailAddress = enterprisedatasource.email_enterprise ?? "",
                FacebookAddress = enterprisedatasource.facebook ?? "",
                TwitterAddress = enterprisedatasource.twitter ?? "",
                LinkedInAddress = enterprisedatasource.linkedin ?? "",
                PhoneNumber = enterprisedatasource.phone ?? "",
                OwnEnterprise = enterprisedatasource.own_enterprise,
                EnterpriseName = enterprisedatasource.enterprise_name ?? "",
                PhotoAddress = enterprisedatasource.photo ?? "",
                Description = enterprisedatasource.description ?? "",
                City = enterprisedatasource.city ?? "",
                Country = enterprisedatasource.country ?? "",
                CompanyValue = enterprisedatasource.value ?? 0,
                SharePrice = enterprisedatasource.share_price ?? 0,
                Shares = enterprisedatasource.shares ?? 0,
                OwnShares = enterprisedatasource.own_shares ?? 0,
                EnterpriseType = new EnterpriseType()
                {
                    ID = enterprisedatasource.TB_ENTERPRISES_TYPE.id,
                    EnterpriseTypeName = enterprisedatasource.TB_ENTERPRISES_TYPE.enterprise_type_name ?? ""
                }
            };
        }

        /// <summary>
        /// Transforms a <see cref="TB_ENTERPRISES"/> entity into a <see cref="Enterprise"/> object
        /// </summary>
        /// <param name="enterprisedatasource"><see cref="TB_ENTERPRISES"/> to be transformed</param>
        /// <returns></returns>
        private Enterprise EnterpriseTransformer(TB_ENTERPRISES enterprisedatasource)
        {
            // Creates a new Enterprise object based on the data loaded from the database
            return new Enterprise()
            {
                ID = enterprisedatasource.id,
                EmailAddress = enterprisedatasource.email_enterprise ?? "",
                FacebookAddress = enterprisedatasource.facebook ?? "",
                TwitterAddress = enterprisedatasource.twitter ?? "",
                LinkedInAddress = enterprisedatasource.linkedin ?? "",
                PhoneNumber = enterprisedatasource.phone ?? "",
                OwnEnterprise = enterprisedatasource.own_enterprise,
                EnterpriseName = enterprisedatasource.enterprise_name ?? "",
                PhotoAddress = enterprisedatasource.photo ?? "",
                Description = enterprisedatasource.description ?? "",
                City = enterprisedatasource.city ?? "",
                Country = enterprisedatasource.country ?? "",
                CompanyValue = enterprisedatasource.value ?? 0,
                SharePrice = enterprisedatasource.share_price ?? 0,
                EnterpriseType = new EnterpriseType()
                {
                    ID = enterprisedatasource.TB_ENTERPRISES_TYPE.id,
                    EnterpriseTypeName = enterprisedatasource.TB_ENTERPRISES_TYPE.enterprise_type_name ?? ""
                }
            };
        }

        /// <summary>
        /// Transforms a list of <see cref="TB_ENTERPRISES"/> entities into a <see cref="Enterprise"/> list
        /// </summary>
        /// <param name="enterprisesdata">List of <see cref="TB_ENTERPRISES"/> to be transformed</param>
        /// <returns></returns>
        private List<Enterprise> EnterpriseTransformer(List<TB_ENTERPRISES> enterprisesdata)
        {
            // Validates the input parameters
            if (enterprisesdata == null) throw new ArgumentNullException(nameof(enterprisesdata));

            List<Enterprise> enterprises = new List<Enterprise>();

            // Foreach TB_ENTERPRISES entity
            foreach (TB_ENTERPRISES enterprise in enterprisesdata)
            {
                // Converts to Enterprise object
                enterprises.Add(this.EnterpriseTransformer(enterprise));
            }

            return enterprises;
        }
        #endregion
    }
}
