﻿using EnterpriseDOMAIN.Models;
using System.Collections.Generic;

namespace EnterpriseDOMAIN.Interfaces
{
    /// <summary>
    /// Interface for domains responsible for dealing with the <see cref="Enterprise"/> data collection
    /// </summary>
    public interface IEnterpriseDomain
    {
        /// <summary>
        /// Filter the list of <see cref="Enterprise"/> by name
        /// </summary>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        List<Enterprise> FilterByName(string name);

        /// <summary>
        /// Filter the list of <see cref="Enterprise"/> by type
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <returns></returns>
        List<Enterprise> FilterByType(int typeid);

        /// <summary>
        /// Filter the list of <see cref="Enterprise"/> by type and/or name
        /// </summary>
        /// <param name="typeid">Number that represents the type of the enterprise</param>
        /// <param name="name">Enterprise name</param>
        /// <returns></returns>
        List<Enterprise> FilterByTypeAndName(int typeid, string name);

        /// <summary>
        /// Lists all the <see cref="Enterprise"/> objetcs in the respository
        /// </summary>
        /// <returns></returns>
        List<Enterprise> GetAll();

        /// <summary>
        /// Find an <see cref="DetailedEnterprise"/> object by id
        /// </summary>
        /// <param name="id">ID number that identifies the <see cref="DetailedEnterprise"/> objetc</param>
        /// <returns></returns>
        DetailedEnterprise GetById(int id);
    }
}
