﻿using Newtonsoft.Json;

namespace EnterpriseDOMAIN.Models
{
    /// <summary>
    /// Represents an Enterprise object with aditional information about the company shares
    /// </summary>
    public class DetailedEnterprise : Enterprise
    {
        /// <summary>
        /// Number of shares
        /// </summary>
        [JsonProperty(PropertyName = "shares")]
        public int Shares { get; set; }

        /// <summary>
        /// Number o shares that the company owns
        /// </summary>
        [JsonProperty(PropertyName = "own_shares")]
        public int OwnShares { get; set; }
    }
}
