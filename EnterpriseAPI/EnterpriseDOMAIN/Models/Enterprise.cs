﻿using Newtonsoft.Json;

namespace EnterpriseDOMAIN.Models
{
    /// <summary>
    /// Represents an Enterprise object
    /// </summary>
    public class Enterprise
    {
        /// <summary>
        /// Company ID number
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int ID { get; set; }

        /// <summary>
        /// Email Address
        /// </summary>
        [JsonProperty(PropertyName = "email_enterprise")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Facebook Address
        /// </summary>
        [JsonProperty(PropertyName = "facebook")]
        public string FacebookAddress { get; set; }

        /// <summary>
        /// Twitter Address
        /// </summary>
        [JsonProperty(PropertyName = "twitter")]
        public string TwitterAddress { get; set; }

        /// <summary>
        /// LinkedIn Address
        /// </summary>
        [JsonProperty(PropertyName = "linkedin")]
        public string LinkedInAddress { get; set; }

        /// <summary>
        /// Company contact number
        /// </summary>
        [JsonProperty(PropertyName = "phone")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Who ows the company(?)
        /// </summary>
        [JsonProperty(PropertyName = "own_enterprise")]
        public bool OwnEnterprise { get; set; }

        /// <summary>
        /// The name of the company
        /// </summary>
        [JsonProperty(PropertyName = "enterprise_name")]
        public string EnterpriseName { get; set; }

        /// <summary>
        /// Link to a photo of the company
        /// </summary>
        [JsonProperty(PropertyName = "photo")]
        public string PhotoAddress { get; set; }

        /// <summary>
        /// Company Description
        /// </summary>
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Company City
        /// </summary>
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

        /// <summary>
        /// Company value
        /// </summary>
        [JsonProperty(PropertyName = "value")]
        public float CompanyValue { get; set; }

        /// <summary>
        /// Shave price
        /// </summary>
        [JsonProperty(PropertyName = "share_price")]
        public float SharePrice { get; set; }

        /// <summary>
        /// Enterprise Type info
        /// </summary>
        [JsonProperty(PropertyName = "enterprise_type")]
        public EnterpriseType EnterpriseType { get; set;}
    }
}
