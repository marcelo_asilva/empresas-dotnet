﻿using Newtonsoft.Json;

namespace EnterpriseDOMAIN.Models
{
    /// <summary>
    /// Represents the type of an Enterprise
    /// </summary>
    public class EnterpriseType
    {
        // Properties
        #region Properties
        [JsonProperty(PropertyName = "id")]
        /// <summary>
        /// Type ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Type name/description
        /// </summary>
        [JsonProperty(PropertyName = "enterprise_type_name")]
        public string EnterpriseTypeName {get; set;}
        #endregion
    }
}
