﻿using EnterpriseDOMAIN.Models;
using EnterpriseDOMAIN.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Http;

namespace EnterpriseAPI.Controllers
{
    /// <summary>
    /// Controller that eals with the requests for Enterprise information
    /// </summary>
    [Route("api/v1/enterprises")]
    public class EnterpriseController : ApiController
    {
        // Variables
        #region Variables
        /// <summary>
        /// Domain responsible for dealing with the <see cref="Enterprise"/> data
        /// </summary>
        private IEnterpriseDomain _EnterpriseDomain;
        #endregion

        // Class Constructors
        #region Constructors
        /// <summary>
        /// Constructor using dependency injection on the domain
        /// </summary>
        /// <param name="enterpriseDomain">Domain responsible for dealing with the <see cref="Enterprise"/> data</param>
        public EnterpriseController(IEnterpriseDomain enterpriseDomain)
        {
            this._EnterpriseDomain = enterpriseDomain;
        }
        #endregion

        // Public Methods
        #region Public
        /// <summary>
        /// Returns all the registered Enterprises
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult Get()
        {
            // Lists all the enteprises
            Dictionary<string, List<Enterprise>> jsonreturn = new Dictionary<string, List<Enterprise>>();
            jsonreturn.Add("enterprises", this._EnterpriseDomain.GetAll());

            // Return list in JSON format
            return JsonText(JsonConvert.SerializeObject(jsonreturn));
        }

        /// <summary>
        /// Returns all the registered Enterprises that match the filter parameters
        /// </summary>
        /// <param name="enterprise_types">Number that represents a type of enterprise</param>
        /// <param name="name">Name of the enterprise</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Filter(int enterprise_types, string name)
        {
            // Filter the enterprises by type and name
            Dictionary<string, List<Enterprise>> jsonreturn = new Dictionary<string, List<Enterprise>>();
            jsonreturn.Add("enterprises", this._EnterpriseDomain.FilterByTypeAndName(enterprise_types, name));

            // Return the filtered Enterprises list in JSON format
            return JsonText(JsonConvert.SerializeObject(jsonreturn));
        }

        /// <summary>
        /// Returns all the registered Enterprises that match the filter parameters
        /// </summary>
        /// <param name="enterprise_types">Number that represents a type of enterprise</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Filter(int enterprise_types)
        {
            // Filter the enterprises by type
            Dictionary<string, List<Enterprise>> jsonreturn = new Dictionary<string, List<Enterprise>>();
            jsonreturn.Add("enterprises", this._EnterpriseDomain.FilterByType(enterprise_types));

            // Return the filtered Enterprises list in JSON format
            return JsonText(JsonConvert.SerializeObject(jsonreturn));
        }

        /// <summary>
        /// Returns all the registered Enterprises that match the filter parameters
        /// </summary>
        /// <param name="name">Name of the enterprise</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Filter(string name)
        {
            // Filter the enterprises by name
            Dictionary<string, List<Enterprise>> jsonreturn = new Dictionary<string, List<Enterprise>>();
            jsonreturn.Add("enterprises", this._EnterpriseDomain.FilterByName(name));

            // Return the filtered Enterprises list in JSON format
            return JsonText(JsonConvert.SerializeObject(jsonreturn));
        }

        /// <summary>
        /// Return an Enterprise that the specified ID
        /// </summary>
        /// <param name="id">Enterprise ID</param>
        /// <returns></returns>
        [Route("api/v1/enterprises/{id}")]
        public IHttpActionResult Get(int id)
        {
            // Try to find an Enterprise by the ID
            DetailedEnterprise enterprise = this._EnterpriseDomain.GetById(id);
            if (enterprise == null)
            {
                return Json(new NotFoundError());
            }

            // Return Enterprise data in JSON format
            return JsonText(JsonConvert.SerializeObject(new EnterpriseResult(enterprise)));
        }
        #endregion

        // Internal Methods
        #region Internal
        /// <summary>
        /// Return JSON content compatible with the <see cref="IHttpActionResult"/> interface
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        protected internal virtual JsonTextActionResult JsonText(string jsonText)
        {
            return new JsonTextActionResult(Request, jsonText);
        }
        #endregion

        // Auxliary classes
        #region Aux
        // EnterpriseResult
        #region EnterpriseResult
        /// <summary>
        /// Encapsulates the a <see cref="DetailedEnterprise"/> element to be return by the Get method
        /// </summary>
        private class EnterpriseResult
        {
            /// <summary>
            /// The Enterprise data
            /// </summary>
            public readonly DetailedEnterprise enterprise;

            /// <summary>
            /// Indicate if the search for the Enterprise was successful
            /// </summary>
            public readonly bool sucess;

            /// <summary>
            /// Emcapsulate the <see cref="DetailedEnterprise"/> element
            /// </summary>
            /// <param name="enterprise"></param>
            public EnterpriseResult(DetailedEnterprise enterprise)
            {
                this.enterprise = enterprise;
                this.sucess = enterprise != null;
            }
        }
        #endregion

        // NotFoundError
        #region NotFoundError
        /// <summary>
        /// Repreents the 404 - Not Found Error message
        /// </summary>
        private class NotFoundError
        {
            /// <summary>
            /// Status Code
            /// </summary>
            public readonly string status = "404";

            /// <summary>
            /// Message
            /// </summary>
            public readonly string error = "Not found";

            /// <summary>
            /// Defaul constructor
            /// </summary>
            public NotFoundError()
            {
                //
            }
        }
        #endregion
        #endregion
    }
}