﻿using System.Web.Mvc;

namespace EnterpriseAPI.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Home Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Enterprise Web API";

            // Data used to show how to use the API
            {
                string baseURL = "http://" + Request.Url.Authority + "/api/v1/enterprises";
                ViewBag.GetAll = baseURL;

                ViewBag.GetById = baseURL + "/{id}";
                ViewBag.GetByIdExample = baseURL + "/7";

                ViewBag.FilterByType = baseURL + "?enterprise_types={type}";
                ViewBag.FilterByTypeExample = baseURL + "?enterprise_types=9";

                ViewBag.FilterByName = baseURL + "?name={name}";
                ViewBag.FilterByNameExample = baseURL + "?name=CGS";

                ViewBag.FilterByTypeAndName = baseURL + "?enterprise_types={type}&name={name}";
                ViewBag.FilterByTypeAndNameExample = baseURL + "?enterprise_types=14&name=ArchDaily";
            }

            return View();
        }
    }
}
