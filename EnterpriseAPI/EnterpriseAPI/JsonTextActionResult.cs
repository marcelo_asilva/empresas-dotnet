﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace EnterpriseAPI
{
    /// <summary>
    /// Emcapsulates JSON text as an <see cref="IHttpActionResult"/>
    /// </summary>
    public class JsonTextActionResult : IHttpActionResult
    {
        // Properties
        #region Properties
        /// <summary>
        /// The request message that will contain the JSON content
        /// </summary>
        public HttpRequestMessage Request { get; }

        /// <summary>
        /// The JSON content
        /// </summary>
        public string JsonText { get; }
        #endregion

        // Constructors
        #region Constructor
        /// <summary>
        /// Creates the custom JSON result capsule
        /// </summary>
        /// <param name="request"></param>
        /// <param name="jsonText"></param>
        public JsonTextActionResult(HttpRequestMessage request, string jsonText)
        {
            Request = request;
            JsonText = jsonText;
        }
        #endregion

        // Public Methods
        #region Public
        /// <summary>
        /// Executes the request and returns the response in an Async way
        /// </summary>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        /// <summary>
        /// Executes the request and returns the response
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Execute()
        {
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(JsonText, Encoding.UTF8, "application/json");

            return response;
        }
        #endregion
    }
}