# LEIAME #

### BANCO DE DADOS ###

Os dados consumidos pela API estão localizados em um arquivo .mdf salvo em __"empresas-dotnet\EnterpriseAPI\EnterpriseAPI\App_Data\DB_Enterprise.mdf"__. A aplicação aponta para esse banco de dados através da connectionstring __DB_EnterpriseEntities__ que está no __Web.config__ da API.

Por favor alterar o valor da propriedade __attachdbfilename__ da connectionstring para refletir o caminho em disco onde o arquivo __DB_Enterprise.mdf__ se encontra na sua máquina antes de executar a solução.

### ESCOPO DO PROJETO ###

* A funcionalidade de Login e verificação de acesso de usuários registrados __não foi desenvolvida__, com isso a API possuí __acesso público__ aos métodos de busca e filtro de empresas.
* API criada usando .NET Framework 4.7.2
* Listagem de todas as Empresas através do caminho {{dev_host}}/api/{{api_version}}/enterprises
* Detalhamento de Empresas através do caminho {{dev_host}}/api/{{api_version}}/enterprises/{{id}}
* Filtro de Empresas por nome e tipo através do caminho {{dev_host}}/api/{{api_version}}/enterprises?enterprise_types={{typeid}}&name={{name}}

### Informações sobre o ambiente de desenvolvimento ###

* Windows 10 Pro 64 bits v1903
* Visual Studio Community 2019 v16.2.3
* .NET Framework 4.7.2
* C# 7.3
* Testes realizados com Postman 7.7.3